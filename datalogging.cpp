#include "datalogging.hpp"
#include "util_rtc.hpp"

module_logger("datalog");

void datalog_queue::new_packet()
{
    auto last = queue.end();
    if (last != queue.begin())
    {
        --last;
        last->commit = true;
    }
    queue.emplace_back();
    ldebug("new packet. now i have {} packets in queue", queue.size());
}

void datalog_queue::trim_old_packets()
{
    while (queue.begin() != queue.end())
    {
        if (! queue.begin()->commit)
        {
            return;
        }
        queue.erase(queue.begin());
    }
}

datalog_packet *datalog_queue::current_packet()
{
    auto last = queue.end();
    if (last == queue.begin())
    {
        queue.emplace_back();
    }
    return &queue.back();
}

void datalog_queue::post(const lidar_pt &data, bool begin_new_line)
{
    auto &l = current_packet()->lidar_scanlines;
    if (l.begin() == l.end() || begin_new_line)
    {
        l.emplace_back(lidar_scanline{m_height, rtc_get_ms()});
    }
    l.back().samples.emplace_back(data);
}

void datalog_queue::post(const accel_data &data)
{
    current_packet()->accel_samples.emplace_back(accel_data{m_height, rtc_get_ms(), data.x, data.y, data.z});
}

void datalog_queue::set_current_height(double h)
{
    m_height = h;
}


void to_stream(stream_packer &out, const datalog_queue &o)
{
    out.pack_map(1);
    out.pack("queue");
    unsigned int ctr = 0;
    for (auto &i: o.queue)
    {
        if (i.commit)
        {
            ++ctr;
        }
    }
    out.pack_array(ctr);
    for (auto &i: o.queue)
    {
        if (i.commit)
        {
            to_stream(out, i);
        }
    }
}

void to_stream(stream_packer &out, const datalog_packet &o)
{
    out.pack_map(4);
    out.pack("t");
    out.pack_uint64(o.time);
    out.pack("c");
    out.pack_uint64(o.count);
    out.pack("accel_samples");
    out.pack_array(o.accel_samples.size());
    for (auto &i: o.accel_samples)
    {
        to_stream(out, i);
    }
    out.pack("lidar_scanlines");
    out.pack_array(o.lidar_scanlines.size());
    for (auto &i: o.lidar_scanlines)
    {
        to_stream(out, i);
    }
}

void to_stream(stream_packer &out, const accel_data &o)
{
    out.pack_map(5);
    out.pack("h");
    out.pack_uint64(o.height);
    out.pack("t");
    out.pack_uint64(o.time);
    out.pack("x");
    out.pack_uint64(o.x);
    out.pack("y");
    out.pack_uint64(o.y);
    out.pack("z");
    out.pack_uint64(o.z);
}

void to_stream(stream_packer &out, const lidar_scanline &o)
{
    out.pack_map(3);
    out.pack("h");
    out.pack_uint64(o.height);
    out.pack("t");
    out.pack_uint64(o.time);
    out.pack("samples");
    out.pack_array(o.samples.size());
    for (auto &i: o.samples)
    {
        to_stream(out, i);
    }
}

void to_stream(stream_packer &out, const lidar_pt &o)
{
    out.pack_map(2);
    out.pack("a");
    out.pack_uint64(o.angle);
    out.pack("d");
    out.pack_uint64(o.distance);
}
