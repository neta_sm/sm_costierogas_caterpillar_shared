#include <boost/bimap.hpp>
#include "types.hpp"
#include "logging.hpp"


module_logger("types");

template <typename L, typename R>
boost::bimap<L, R>
make_bimap(std::initializer_list<typename boost::bimap<L, R>::value_type> list)
{
    return boost::bimap<L, R>(list.begin(), list.end());
}

static const auto camera_slot_map = make_bimap<camera_slot, std::string>
({
     {camera_slot::back, "back"},
     {camera_slot::front, "front"},
     {camera_slot::orient, "orient"},
 });

std::ostream& operator<<(std::ostream& os, camera_slot const& v){
    auto &map = camera_slot_map.left;
    auto f = map.find(v);
    if (f == map.end())
    {
        lwarning("unknown camera_slot value {}", static_cast<int>(v));
        throw std::bad_cast();
    }
    os << f->second;
    return os;
}

std::istream& operator >>(std::istream &is, camera_slot &v)
{
    std::string s;
    is >> s;
    auto &map = camera_slot_map.right;
    auto f = map.find(s);
    if (f == map.end())
    {
        lwarning("unknown camera_slot name {}", s);
        throw std::bad_cast();
    }
    v = f->second;
    return is;
}




static const auto propulsion_slot_map = make_bimap<propulsion_slot, std::string>
({
     {propulsion_slot::axial, "axial"},
     {propulsion_slot::left, "left"},
     {propulsion_slot::right, "right"},
 });

std::ostream& operator<<(std::ostream& os, propulsion_slot const& v){
    auto &map = propulsion_slot_map.left;
    auto f = map.find(v);
    if (f == map.end())
    {
        lwarning("unknown propulsion_slot value {}", static_cast<int>(v));
        throw std::bad_cast();
    }
    os << f->second;
    return os;
}

std::istream& operator >>(std::istream &is, propulsion_slot &v)
{
    std::string s;
    is >> s;
    auto &map = propulsion_slot_map.right;
    auto f = map.find(s);
    if (f == map.end())
    {
        lwarning("unknown propulsion_slot name {}", s);
        throw std::bad_cast();
    }
    v = f->second;
    return is;
}


static const auto camera_status_map = make_bimap<camera_status, std::string>
({
     {camera_status::idle,         "idle"},
     {camera_status::armed,        "armed"},
     {camera_status::free_running, "free_running"},
     {camera_status::error,        "error"},
 });

std::ostream& operator<<(std::ostream& os, camera_status const& v){
    auto &map = camera_status_map.left;
    auto f = map.find(v);
    if (f == map.end())
    {
        lwarning("unknown propulsion_slot value {}", static_cast<int>(v));
        throw std::bad_cast();
    }
    os << f->second;
    return os;
}

