#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <string>
#include <vector>
#include <memory>
#include <functional>
#include <iostream>
#include <json.hpp>
#include <msgpack.hpp>
#include "logging.hpp"
#if SERVER_SIDE
#include "../status_fwd.hpp"
#endif


#define FLAG_COMMAND            0x00
#define FLAG_REPLY_ACK          (0x01<<16)
#define FLAG_REPLY_COMPLETED    (0x02<<16)
#define FLAG_REPLY_ERROR        (0x03<<16)

using json = nlohmann::json;
using bytes = std::vector<std::uint8_t>;

#if SERVER_SIDE
struct json_or_bytes {
    enum { is_json, is_bytes } type;
    json json_data;
    bytes bytes_data;

    json_or_bytes(json &&data);
    json_or_bytes(bytes &&data);
    json_or_bytes() = delete;
    json_or_bytes(json_or_bytes const &) = delete;
    void operator=(json_or_bytes const &x) = delete;
    json_or_bytes(json_or_bytes &&o) = default;
};

#endif

enum cmdid {
    CMD_PING = 0x00,
    CMD_GET_STATUS = 0x01,
    CMD_PROPELLER = 0x02,
    CMD_LED_SET = 0x03,
    CMD_VISUALS_SET = 0x04,
    CMD_PHOTO = 0x05,
    CMD_VIDEO = 0x06,
    CMD_POWER = 0x07,
    CMD_VIDEO_FRAME = 0x08,
    CMD_MOTOR_INC = 0x09,
    CMD_MOTOR_GOTO = 0x10,
    CMD_DATALOGGING_START = 0x0A,
    CMD_DATALOGGING_FETCH = 0x0B,
    CMD_DATALOGGING_STOP = 0x0C,
    CMD_DATALOGGING_GET_STATUS = 0x0D,
    CMD_MOTOR_SET_ZERO = 0x11,
    CMD_LIDAR_SET = 0x12,
};

enum cmdtype {
    cmd_req,
    cmd_reply_ack,
    cmd_reply_completed,
    cmd_reply_error,
};

enum cmd_error {
    err_ok = 0x00,
    err_bad_params = 0x01,
    err_timeout = 0x02,
    err_disconnected = 0x03,
    err_no_datalog_service = 0x04,
    err_internal = 0xff,
};


struct apiconn_t;

struct base_cmd: std::enable_shared_from_this<base_cmd>  {
    base_cmd(unsigned int _id, unsigned int _token, const json &_payload):
        id(_id),
        token(_token),
        payload(_payload)
    {}
    base_cmd(unsigned int _id, unsigned int _token):
        id(_id),
        token(_token)
    {}
    base_cmd(unsigned int _id):
        id(_id),
        token(base_cmd::new_token())
    {}
    base_cmd(unsigned int _id, const json &_payload):
        id(_id),
        token(base_cmd::new_token()),
        payload(_payload)
    {}
    base_cmd(const base_cmd&) = delete;

    virtual ~base_cmd() {}
    virtual cmdid get_id() const;
    virtual cmdtype type() const;
    virtual json produce_request() const;
    virtual json produce_request_payload() const;
#if SERVER_SIDE
    virtual json produce_ack_reply() const;
    virtual const json &produce_reply_payload() const;
    virtual json_or_bytes produce_completed_reply() const;
    virtual json produce_error_reply(cmd_error code, const std::string &txt) const;
    virtual bool runs_immediately(status_t &status) const;
    virtual void execute(apiconn_t &status);
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();
#endif

    unsigned int id;
    unsigned int token;
    json payload;

    static unsigned int new_token();
#if SERVER_SIDE
    std::weak_ptr<apiconn_t> session_wp;
    void send_async_message(const base_cmd &payload);
#endif
};

std::ostream &operator<<(std::ostream &os, const base_cmd &o);
std::ostream &operator<<(std::ostream &os, const cmdid &v);
std::ostream &operator<<(std::ostream &os, const cmdtype &v);
std::ostream &operator<<(std::ostream &os, const cmd_error &v);



struct cmd_ping: base_cmd {
    using base_cmd::base_cmd;
    cmd_ping():
        base_cmd(CMD_PING)
    {}
};

struct cmd_get_status: base_cmd {
    using base_cmd::base_cmd;
    cmd_get_status():
        base_cmd(CMD_GET_STATUS)
    {}

#if SERVER_SIDE
    virtual void execute(apiconn_t &apiconn);
#endif
};


struct cmd_propeller: base_cmd {
    using base_cmd::base_cmd;
    cmd_propeller():
        base_cmd(CMD_PROPELLER)
    {}

#if SERVER_SIDE
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};


struct cmd_led_set: base_cmd {
    using base_cmd::base_cmd;
    cmd_led_set():
        base_cmd(CMD_LED_SET)
    {}

#if SERVER_SIDE
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};


struct cmd_visuals_set: base_cmd {
    using base_cmd::base_cmd;
    cmd_visuals_set():
        base_cmd(CMD_VISUALS_SET)
    {}

#if SERVER_SIDE
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};


struct cmd_photo: base_cmd {
    using base_cmd::base_cmd;
    cmd_photo():
        base_cmd(CMD_PHOTO)
    {}

#if SERVER_SIDE
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};


struct cmd_video: base_cmd {
    using base_cmd::base_cmd;
    cmd_video():
        base_cmd(CMD_VIDEO)
    {}

#if SERVER_SIDE
    virtual void execute(apiconn_t &apiconn);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};


struct cmd_power: base_cmd {
    using base_cmd::base_cmd;
    cmd_power():
        base_cmd(CMD_POWER)
    {}

#if SERVER_SIDE
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};


struct cmd_video_frame: base_cmd {
    using base_cmd::base_cmd;
    cmd_video_frame():
        base_cmd(CMD_VIDEO_FRAME)
    {}

#if SERVER_SIDE
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};

struct cmd_motor_inc: base_cmd {
    using base_cmd::base_cmd;
    cmd_motor_inc():
        base_cmd(CMD_MOTOR_INC)
    {}

#if SERVER_SIDE
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};

struct cmd_motor_goto: base_cmd {
    using base_cmd::base_cmd;
    cmd_motor_goto():
        base_cmd(CMD_MOTOR_GOTO)
    {}

#if SERVER_SIDE
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};

struct cmd_motor_set_zero: base_cmd {
    using base_cmd::base_cmd;
    cmd_motor_set_zero():
        base_cmd(CMD_MOTOR_SET_ZERO)
    {}

#if SERVER_SIDE
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};

struct cmd_lidar_set: base_cmd {
    using base_cmd::base_cmd;
    cmd_lidar_set():
        base_cmd(CMD_LIDAR_SET)
    {}

#if SERVER_SIDE
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};

struct cmd_datalogging_start: base_cmd {
    using base_cmd::base_cmd;
    cmd_datalogging_start():
        base_cmd(CMD_DATALOGGING_START)
    {}

#if SERVER_SIDE
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};

struct cmd_datalogging_stop: base_cmd {
    using base_cmd::base_cmd;
    cmd_datalogging_stop():
        base_cmd(CMD_DATALOGGING_STOP)
    {}

#if SERVER_SIDE
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};

struct cmd_datalogging_fetch: base_cmd {
    using base_cmd::base_cmd;
    cmd_datalogging_fetch():
        base_cmd(CMD_DATALOGGING_FETCH)
    {}

#if SERVER_SIDE
    virtual json_or_bytes produce_completed_reply() const;
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    mutable bytes raw_data;
    std::string msg;
#endif
};

struct cmd_datalogging_get_status: base_cmd {
    using base_cmd::base_cmd;
    cmd_datalogging_get_status():
        base_cmd(CMD_DATALOGGING_GET_STATUS)
    {}

#if SERVER_SIDE
    virtual void execute(status_t &status);
    virtual cmd_error fault_code();
    virtual std::string fault_msg();

    cmd_error fault = err_ok;
    std::string msg;
#endif
};



typedef std::shared_ptr<base_cmd> cmd_p;

cmd_p message_from_object(const msgpack::object &o);
void data_from_message(bytes &data, const json &msg);

#endif // COMMANDS_HPP
