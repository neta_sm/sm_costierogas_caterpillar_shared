#ifndef TYPES_HPP
#define TYPES_HPP 1

#include <iostream>

enum class camera_slot {
    front,
    back,
    orient,
};

enum class propulsion_slot {
    axial,
    left,
    right,
};

enum class camera_status {
    idle,
    armed,
    free_running,
    error,
};


std::ostream& operator<<(std::ostream& os, camera_slot const& v);
std::istream& operator>>(std::istream& is, camera_slot &v);

std::ostream& operator<<(std::ostream& os, propulsion_slot const& v);
std::istream& operator>>(std::istream& is, propulsion_slot &v);

std::ostream& operator<<(std::ostream& os, camera_status const& v);

#endif
