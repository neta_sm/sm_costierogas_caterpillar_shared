#include "commands.hpp"
#include <atomic>
#include <iostream>
#if SERVER_SIDE
#  include "servant.hpp"
#endif

module_logger("commands");

static std::atomic<unsigned int> token_ctr(0);

unsigned int base_cmd::new_token()
{
    return ++token_ctr;
}

cmdid base_cmd::get_id() const
{
    return (cmdid)(id & 0xff);
}

cmdtype base_cmd::type() const
{
    auto b = id & (0x3 << 16);
    switch (b) {
    case FLAG_COMMAND: return cmd_req;
    case FLAG_REPLY_ACK: return cmd_reply_ack;
    case FLAG_REPLY_COMPLETED: return cmd_reply_completed;
    case FLAG_REPLY_ERROR: return cmd_reply_error;
    }
    return cmd_reply_error;
}

json base_cmd::produce_request() const
{
    auto res = json{
                id,
                token
    };
    auto payload = produce_request_payload();
    if (!payload.empty())
    {
        res.push_back(payload);
    }
    return res;
}

json base_cmd::produce_request_payload() const
{
    return payload;
}


#if SERVER_SIDE

json_or_bytes::json_or_bytes(json &&data):
    type(is_json),
    json_data(std::move(data))
{
}

json_or_bytes::json_or_bytes(bytes &&data):
    type(is_bytes),
    bytes_data(std::move(data))
{
}


json base_cmd::produce_ack_reply() const
{
    return json{
                id | FLAG_REPLY_ACK,
                token
    };
}

json_or_bytes base_cmd::produce_completed_reply() const
{
    auto payload = produce_reply_payload();
    auto res = json{
                id | FLAG_REPLY_COMPLETED,
                token
    };
    if (!payload.empty())
    {
        res.push_back(payload);
    }
    return res;
}

const json &base_cmd::produce_reply_payload() const
{
    return payload;
}

json base_cmd::produce_error_reply(cmd_error code, const std::string &txt) const {
    return json{
                id | FLAG_REPLY_ERROR,
                token,
                {
                    { "error_code", code },
                    { "error_txt", txt },
                }
    };
}
bool base_cmd::runs_immediately(status_t &status) const
{
    return true;
}

void base_cmd::execute(apiconn_t &apiconn)
{
    execute(*apiconn.get_status());
}

void base_cmd::execute(status_t &status)
{}

cmd_error base_cmd::fault_code()
{
    return err_ok;
}
std::string base_cmd::fault_msg()
{
    static const std::string nothing("");
    return nothing;
}

void base_cmd::send_async_message(const base_cmd &payload)
{
    auto session_p = session_wp.lock();
    if (session_p == nullptr)
    {
        return;
    }
    auto data = json::to_msgpack(payload.produce_request());
    session_p->write(&data.front(), data.size());
}


#endif // SERVER_SIDE

std::ostream &operator<<(std::ostream &os, const base_cmd &o)
{
    os << (cmdid)(0xffff & o.id) << "(" << o.token << ", " << o.type() << ")";
    return os;
}
static const auto cmdid_map = std::map<cmdid, std::string>
{
    {CMD_PING       , "PING"},
    {CMD_GET_STATUS , "GET_STATUS"},
    {CMD_PROPELLER  , "PROPELLER"},
    {CMD_LED_SET    , "CMD_LED_SET"},
    {CMD_VISUALS_SET, "CMD_VISUALS_SET"},
    {CMD_PHOTO      , "CMD_PHOTO"},
    {CMD_VIDEO      , "CMD_VIDEO"},
    {CMD_POWER      , "CMD_POWER"},
    {CMD_VIDEO_FRAME, "CMD_VIDEO_FRAME"},
    {CMD_MOTOR_INC  , "CMD_MOTOR_INC"},
    {CMD_MOTOR_GOTO , "CMD_MOTOR_GOTO"},
    {CMD_MOTOR_SET_ZERO, "CMD_MOTOR_SET_ZERO"},
    {CMD_LIDAR_SET, "CMD_LIDAR_SET"},
    {CMD_DATALOGGING_START, "CMD_DATALOGGING_START"},
    {CMD_DATALOGGING_FETCH, "CMD_DATALOGGING_FETCH"},
    {CMD_DATALOGGING_STOP, "CMD_DATALOGGING_STOP"},
    {CMD_DATALOGGING_GET_STATUS, "CMD_DATALOGGING_GET_STATUS"},
};

std::ostream &operator<<(std::ostream& os, const cmdid &v)
{
    auto &map = cmdid_map;
    auto f = map.find(v);
    if (f == map.end())
    {
        os << "unknown";
    } else {
        os << f->second;
    }
    return os;
}
static const auto cmdtype_map = std::map<cmdtype, std::string>
{
     {cmd_req,             "req"},
     {cmd_reply_ack,       "reply_ack"},
     {cmd_reply_completed, "reply_completed"},
     {cmd_reply_error,     "reply_error"},
};
std::ostream &operator<<(std::ostream& os, const cmdtype &v)
{
    auto &map = cmdtype_map;
    auto f = map.find(v);
    if (f == map.end())
    {
        os << "unknown";
    } else {
        os << f->second;
    }
    return os;
}
static const auto cmd_error_map = std::map<cmd_error, std::string>
{
    {err_ok,             "ok"},
    {err_timeout,     "timeout"},
    {err_bad_params,     "bad_params"},
    {err_disconnected,   "disconnected"},
    {err_internal,       "err_internal"},
};
std::ostream &operator<<(std::ostream& os, const cmd_error &v)
{
    auto &map = cmd_error_map;
    auto f = map.find(v);
    if (f == map.end())
    {
        os << "unknown";
    } else {
        os << f->second;
    }
    return os;
}

std::string to_string(const json &data)
{
    std::stringstream ss;
    ss << data;
    return ss.str();
}

std::string to_string(const msgpack::object &data)
{
    std::stringstream ss;
    ss << data;
    return ss.str();
}



template<typename cmd_class> cmd_p mkcommand(std::uint32_t id, std::uint32_t token, const json &payload)
{
    return std::dynamic_pointer_cast<base_cmd>(std::make_shared<cmd_class>(id, token, payload));
}

static void transmute(json &out, const msgpack::object &o)
{
    switch (o.type)
    {
    case msgpack::type::MAP:
    {
        auto i = o.via.map.ptr;
        auto end = i+o.via.map.size;
        for (; i != end; ++i)
        {
            json jv;
            transmute(jv, i->val);
            switch (i->key.type) {
            case msgpack::type::BIN:
            case msgpack::type::STR:
                out.emplace(i->key.as<std::string>(), jv);
                break;
//            case msgpack::type::BOOLEAN:
//                out.emplace(i->key.as<bool>(), jv);
//                break;
            case msgpack::type::POSITIVE_INTEGER:
                out.emplace(std::to_string(i->key.as<unsigned int>()), jv);
                break;
            case msgpack::type::NEGATIVE_INTEGER:
                out.emplace(std::to_string(i->key.as<int>()), jv);
                break;
//            case msgpack::type::FLOAT32:
//            case msgpack::type::FLOAT64:
//                out.emplace(i->key.as<double>(), jv);
//                break;
            default:
                lerror("unsupported Msgpack type as object key: {}", i->key.type);
            }
        }
    }
        break;
    case msgpack::type::ARRAY:
    {
        auto i = o.via.array.ptr;
        auto end = i+o.via.array.size;
        for (; i != end; ++i)
        {
            switch (i->type) {
            case msgpack::type::BIN:
            case msgpack::type::STR:
                out.emplace_back(i->as<std::string>());
                break;
            case msgpack::type::BOOLEAN:
                out.emplace_back(i->as<bool>());
                break;
            case msgpack::type::POSITIVE_INTEGER:
                out.emplace_back(i->as<unsigned long int>());
                break;
            case msgpack::type::NEGATIVE_INTEGER:
                out.emplace_back(i->as<long int>());
                break;
            case msgpack::type::FLOAT32:
            case msgpack::type::FLOAT64:
                out.emplace_back(i->as<double>());
                break;
            default:
                json j;
                transmute(j, *i);
                out.emplace_back(j);
            }
        }
    }
        break;
    case msgpack::type::BIN:
    case msgpack::type::STR:
        out = o.as<std::string>();
        break;
    case msgpack::type::NIL:
        out = nullptr;
        break;
    case msgpack::type::BOOLEAN:
        out = o.as<bool>();
        break;
    case msgpack::type::POSITIVE_INTEGER:
        out = o.as<unsigned long int>();
        break;
    case msgpack::type::NEGATIVE_INTEGER:
        out = o.as<long int>();
        break;
    case msgpack::type::FLOAT32:
    case msgpack::type::FLOAT64:
        out = o.as<double>();
        break;
     default:
        lerror("unsupported Msgpack object type {}", o.type);
    }
}

cmd_p message_from_object(const msgpack::object &o)
{
    //ltrace("processing structure: {}", to_string(o));

    if (o.type != msgpack::type::ARRAY)
    {
        throw std::runtime_error("message structure must be an array");
    }
    if (o.via.array.size != 2 && o.via.array.size != 3)
    {
        throw std::runtime_error("message structure array must be 2 or 3-sized");
    }
    auto &oid = o.via.array.ptr[0];
    unsigned int id;
    try {
        oid.convert(id);
    } catch (...)
    {
        throw std::runtime_error("message command id field at index 0 must be an uint");
    }

    auto &otoken = o.via.array.ptr[1];
    unsigned int token;
    try {
        otoken.convert(token);
    } catch(...) {
        throw std::runtime_error("message token field at index 1 must be an uint");
    }

    json payload;
    if (o.via.array.size > 2)
    {
        auto &opayload = o.via.array.ptr[2];
        if (opayload.type != msgpack::type::MAP)
        {
            throw std::runtime_error("message payload field at index 2, if specified, must be an object");
        }
        transmute(payload, opayload);
    }

    switch (id & 0xff) {
    case CMD_PING:                   return mkcommand<cmd_ping>(id, token, payload);
    case CMD_GET_STATUS:             return mkcommand<cmd_get_status>(id, token, payload);
    case CMD_PROPELLER:              return mkcommand<cmd_propeller>(id, token, payload);
    case CMD_LED_SET:                return mkcommand<cmd_led_set>(id, token, payload);
    case CMD_VISUALS_SET:            return mkcommand<cmd_visuals_set>(id, token, payload);
    case CMD_POWER:                  return mkcommand<cmd_power>(id, token, payload);
    case CMD_VIDEO:                  return mkcommand<cmd_video>(id, token, payload);
    case CMD_MOTOR_INC:              return mkcommand<cmd_motor_inc>(id, token, payload);
    case CMD_MOTOR_GOTO:             return mkcommand<cmd_motor_goto>(id, token, payload);
    case CMD_MOTOR_SET_ZERO:         return mkcommand<cmd_motor_set_zero>(id, token, payload);
    case CMD_LIDAR_SET:              return mkcommand<cmd_lidar_set>(id, token, payload);
    case CMD_DATALOGGING_START:      return mkcommand<cmd_datalogging_start>(id, token, payload);
    case CMD_DATALOGGING_FETCH:      return mkcommand<cmd_datalogging_fetch>(id, token, payload);
    case CMD_DATALOGGING_STOP:       return mkcommand<cmd_datalogging_stop>(id, token, payload);
    case CMD_DATALOGGING_GET_STATUS: return mkcommand<cmd_datalogging_get_status>(id, token, payload);

    default:
        lerror("unknown command id: {}", id);
    };
    return nullptr;
}


void data_from_message(bytes &data, const json &msg)
{
    data = json::to_msgpack(msg);
}

