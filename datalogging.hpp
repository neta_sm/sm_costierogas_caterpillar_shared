#ifndef __DATALOGGING_HPP
#define __DATALOGGING_HPP 1

#include <list>
#include <inttypes.h>
#include <sstream>
#include "msgpack.hpp"
#include "commands.hpp"


typedef msgpack::packer<std::stringstream> stream_packer;

struct lidar_pt {
    double angle;
    double distance;
};

struct lidar_scanline {
    double height;
    uint64_t time;
    std::list<lidar_pt> samples;
};

struct accel_data {
    double height;
    uint64_t time;
    double x;
    double y;
    double z;
};

struct datalog_packet {
    uint64_t time;
    uint64_t count;
    std::list<accel_data> accel_samples;
    std::list<lidar_scanline> lidar_scanlines;
    bool commit = false;
};

struct datalog_queue {
    std::list<datalog_packet> queue;
    void new_packet();

    void trim_old_packets();
    void post(const lidar_pt &data, bool begin_new_line);
    void post(const accel_data &data);
    void set_current_height(double h);

private:
    datalog_packet *current_packet();
    uint64_t m_ctr = 0;
    double m_height = 0;
};

void to_stream(stream_packer &out, const datalog_queue &o);
void to_stream(stream_packer &out, const datalog_packet &o);
void to_stream(stream_packer &out, const accel_data &o);
void to_stream(stream_packer &out, const lidar_scanline &o);
void to_stream(stream_packer &out, const lidar_pt &o);


#endif /* __DATALOGGING_HPP */
